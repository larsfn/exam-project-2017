import Part2

### Make some plots!
print("")
print("PLOTS")
#p = figure(width = 500, height = 200)
#df.plot(x = 'dates', y = "FDX")

import plotly
import plotly.plotly as py
import plotly.graph_objs as go
plotfdx = go.Scatter(
    x = df['dates'],
    y = df['FDX'],
    mode = 'lines',
    name = 'FDX'
)

plotgoogl = go.Scatter(
    x = df['dates'],
    y = df['GOOGL'],
    mode = 'lines',
    name = 'GOOGL'
)

plotibm = go.Scatter(
    x = df['dates'],
    y = df['IBM'],
    mode = 'lines',
    name = 'IBM'
)

plotko = go.Scatter(
    x = df['dates'],
    y = df['KO'],
    mode = 'lines',
    name = 'KO'
)


plotms = go.Scatter(
    x = df['dates'],
    y = df['MS'],
    mode = 'lines',
    name = 'MS'
)

plotnok = go.Scatter(
    x = df['dates'],
    y = df['NOK'],
    mode = 'lines',
    name = 'NOK'
)

plotxom = go.Scatter(
    x = df['dates'],
    y = df['XOM'],
    mode = 'lines',
    name = 'XOM'
)

testplot = [plotfdx, plotgoogl, plotibm, plotko, plotms, plotnok, plotxom]

layout = dict(title = 'Selected stocks 2005-01-01 to 2016-12-31',
              xaxis = dict(title = 'Date'),
              yaxis = dict(title = 'Share price (USD)'),
              )

py.plot(testplot, filename='Stocks-over-time')

