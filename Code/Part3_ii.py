####################################
####################################
#               Notes             #
#Date Created: 1/11/17
#Initial author: SMCK
#Last Modified:


                                          #'METHODS APPROACH'

#Not sure how we're supposed to do this. Where should the data come from? How should the simlution work?
##############
# TO DO:
#############


####################################
####################################

import random


class Investors(object)
    def __init__(self, budget, type, buy_date, df, ):
        self.budget = budget
        self.type = type

    #Build aggressive method here
    def aggressive_strategy(self, stocks, buy_date, df) #not sure where the data for this should come from? Part 2?
        while budget > 100:
            ticker = random.sample(stocks, 1)  # Not sure how this random function works- do we need to assign exact probabilities, or can we just use the defaults?
            print("Aggressive investor will buy:", ticker)

            buy_price = df.at[buy_date, ticker]

            # Max number of possible stocks that can be bought with remaining budget
            max_number = int(budget / buy_price)

            number_of_stocks = random.randint(0, max_number)
            print("Number of stocks bought:", number_of_stocks)

            cost_stocks = number_of_stocks * buy_price
            budget = budget - cost_stocks

    #Build Defensive method here
    def defensive_strategy(self, price):
        while budget >= price:
            bondtype = random.choice(1,2)
            if bondtype == 1: # 1 is for short term bonds
                budget = budget - price
            elif bondtype == 2: #2 is for long term bonds
                budget = budget - price

    #Build Mixed method here
    types = ["Bonds", "Stocks"]
    type = random.choice(types)
    if type == "Bonds":
        print("Mixed investor will buy Bonds")
        # Same rules apply as defensive investor?
        defensive_strategy(self, Short_Bonds, Long_Bonds)

    elif type == "stocks":
        print("Mixed investor will buy Stocks")

        # Same rules apply as for aggressive strategy
        aggressive_strategy(self, stocks, buy_date, df)


    ########################
    #Select relevant method
    #######################
    if type = "aggressive":
        Investors.aggressive_strategy()
    elif type = "mixed":
        Investors.mixed_strategy()
    else:  #for defensive
        Investors.defensive_strategy()
