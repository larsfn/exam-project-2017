# Stocks
import pandas_datareader.data as web
import datetime


start = datetime.datetime(2005, 1, 1)  # set start date
end = datetime.datetime(2016, 12, 31)  # set end date

stocks = [ 'GOOGL', 'FDX', 'XOM', 'KO', 'NOK', 'MS', 'IBM' ]  # save list of stock tickers
pnl = web.DataReader(stocks, 'yahoo', start, end)  # extract stock data from yahoo, for the relevant period

df = pnl.to_frame().unstack(level = 1)  # reshape from panel data to dataframe
df.columns = df.columns.swaplevel(0, 1)
df.info()

df = df.iloc[ : , 7:14]  # extract the 7 columns of close prices for our companies
df.info()

dates = [ ]  # these lines add date as a 'manipulable' item
for x in range(len(df)):
    newdate = str(df.index[x])
    newdate = newdate[0: 10]
    dates.append(newdate)
df['dates'] = dates
print(df.index[0])
df.index = df['dates']  # rename row index to make it easier to work with
print(df.index[0])


print(df.head(1))  # we'll rename the columns to make it easier to work with them
df.columns = ['FDX', 'GOOGL', 'IBM', 'KO', 'MS', 'NOK', 'XOM', 'dates']
print("")
print(list(df.columns.values))

print("")
print("new part")


# Function to buy stocks
# If the exchange was closed on the date entered, it will find the closest business day before the entered date
# and take that price. It will do this for up to 3 days before the entered date. This makes it quite long.

def buy_stocks(ticker, buy_date, sell_date):
    delta_buy = datetime.datetime.strptime(buy_date, "%Y-%m-%d")  # convert the string date entered to timedelta
    buy_date_1 = (delta_buy - datetime.timedelta(days=1)).strftime('%Y-%m-%d')  # entered purchasing date - 1 day
    buy_date_2 = (delta_buy - datetime.timedelta(days=2)).strftime('%Y-%m-%d')  # entered purchasing date - 2 days
    buy_date_3 = (delta_buy - datetime.timedelta(days=3)).strftime('%Y-%m-%d')  # entered purchasing date - 3 days
    if buy_date in df.index:
        buy_price = df.at[buy_date, ticker]
    elif buy_date not in df.index and buy_date_1 in df.index:
        buy_price = df.at[buy_date_1, ticker]
    elif buy_date not in df.index and buy_date_2 in df.index:
        buy_price = df.at[buy_date_2, ticker]
    elif buy_date not in df.index and buy_date_3 in df.index:
        buy_price = df.at[buy_date_3, ticker]
    else:
        print("Error: Please enter a buy date between 2005-01-01 and 2016-12-31")
    delta_sell = datetime.datetime.strptime(sell_date, "%Y-%m-%d")  # convert the string date entered to timedelta
    sell_date_1 = (delta_sell - datetime.timedelta(days=1)).strftime('%Y-%m-%d')  # entered sell date - 1 day
    sell_date_2 = (delta_sell - datetime.timedelta(days=2)).strftime('%Y-%m-%d')  # entered sell date - 2 days
    sell_date_3 = (delta_sell - datetime.timedelta(days=3)).strftime('%Y-%m-%d')  # entered sell date - 3 days
    if sell_date in df.index:
        sell_price = df.at[sell_date, ticker]
    elif sell_date not in df.index and sell_date_1 in df.index:
        sell_price = df.at[sell_date_1, ticker]
    elif sell_date not in df.index and sell_date_2 in df.index:
        sell_price = df.at[sell_date_2, ticker]
    elif sell_date not in df.index and sell_date_3 in df.index:
        sell_price = df.at[sell_date_3, ticker]
    else:
        print("Error: Please enter a sell date between 2005-01-01 and 2016-12-31")
    roi = sell_price - buy_price
    print(ticker, "bought", buy_date, "for", buy_price)
    print(ticker, "sold", sell_date, "for", sell_price)
    print("Return:", roi)


# test function with and without blackout dates
buy_stocks('GOOGL', '2016-01-01', '2016-10-01')
buy_stocks('FDX', '2012-01-15', '2012-12-25')
buy_stocks('NOK', '2010-01-20', '2014-12-23')

### Make some plots!
import plotly.plotly as py
import plotly.graph_objs as go
plotfdx = go.Scatter(
    x = df['dates'],
    y = df['FDX'],
    mode = 'lines',
    name = 'FDX'
)

plotgoogl = go.Scatter(
    x = df['dates'],
    y = df['GOOGL'],
    mode = 'lines',
    name = 'GOOGL'
)

plotibm = go.Scatter(
    x = df['dates'],
    y = df['IBM'],
    mode = 'lines',
    name = 'IBM'
)

plotko = go.Scatter(
    x = df['dates'],
    y = df['KO'],
    mode = 'lines',
    name = 'KO'
)


plotms = go.Scatter(
    x = df['dates'],
    y = df['MS'],
    mode = 'lines',
    name = 'MS'
)

plotnok = go.Scatter(
    x = df['dates'],
    y = df['NOK'],
    mode = 'lines',
    name = 'NOK'
)

plotxom = go.Scatter(
    x = df['dates'],
    y = df['XOM'],
    mode = 'lines',
    name = 'XOM'
)

stockplot = [plotfdx, plotgoogl, plotibm, plotko, plotms, plotnok, plotxom]

layout = dict(title = 'Selected stocks 2005-01-01 to 2016-12-31',
              xaxis = dict(title = 'Date'),
              yaxis = dict(title = 'Share price (USD)'),
              )

# in order to print plot, uncomment the line below. The plot will open in a new browser window
stockfig = dict(data = stockplot, layout = layout)
#py.plot(stockfig, filename='Stocks-over-time')