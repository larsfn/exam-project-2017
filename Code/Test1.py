import datetime

string_date = '2016-12-30'
newdate = datetime.datetime.strptime(string_date, "%Y-%m-%d")

newdate1 = (newdate + datetime.timedelta(days=1))


print(newdate1.strftime('%m/%d/%Y'))
print((newdate + datetime.timedelta(days=1)).strftime('%Y-%m-%d'))

print(isinstance((newdate + datetime.timedelta(days=1)).strftime('%Y-%m-%d'), str))