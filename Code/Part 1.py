####################################
####################################
#               Notes             #
#Last modification: 31/10/17 (Halloween...spooky)
# This is a fully functioning script, however, it may need to be adjusted, depending on later requirements.
# The method "set_short_term" and "get_bond_type" are similar to the example on slide 62, day 2 of the lecture notes.
# However it is not clear how useful/necessary they are.
# There is no functionality to STOP the script from running if a bond does not meet the minimum criteria. However, there
# are methods which will CHECK whether a bond meets the relevant criteria.
# The COMPOUND_INTEREST method is the business end of the script. I *think* the formula used to calculate the ROI is
# correct, but we should double check it.
##############
# TO DO:
#############
#  Ideally, it would be cool to simply enter all details into the overall Bond class (i.e. including "Type") and then
# programme an algorithm which assigns it to the correct bond subclass, then checks if it meets all the criteria,
# before finally calling the compound_interest method. I'm not sure if this is even possible (it probably is...)


####################################
####################################


# import items
import matplotlib.pyplot as plt

# Bonds class
class Bonds(object):
    def __init__(self, term, amount, price, i_rate):
        self.term = term  # term invested in
        self.amount = amount  # investment amount
        self.price = price  # minimum price of bond
        self.i_rate = i_rate  # yearly interest rate

    def compound_interest(term, amount,i_rate): #Function for the Bond class, which calculates return on investment, given an interest rate, term time and principal amount.
        rate = 1 + i_rate
        real_term = term - 1
        rate_term = round((1+i_rate) ** (real_term),2)
        return amount * rate_term



    ##########################################
    #This mimiks the 'get alive' status check functionality shown on slide 62, Day 2- I'm not sure how useful it is...
    ##########################################
    def set_short_term(self, short_term):
        self.short_term= short_term

    def get_bond_type(self):
        print("This is a short term bond:")
        return self.short_term
    ##########################################

class Short_Bonds(Bonds): # Subclass for short term bonds

    def min_values_st(self): # A method to check conditions are met for Short Term Bonds.
        print("**Checking Short Term Bond Meets Minimal Values**")
        if self.price < 500:
             return "Short Bond price too low"
        elif self.term < 3:
            return "Short bond term too short"
        elif self.i_rate < 0.01:
            return "Short bond interest rate too small"
        #Need a way to break the (whole) script if one of the above conditions are met
        else:
            return "Short term bond requirements met"

class Long_Bonds(Bonds): #Subclass for long term bonds

    def min_values_lt(self): #A method to check conditions are met for Long Term Bonds
        print("**Checking Long Term Bond Meets Minimlal Values**")
        if self.price < 1000:
            return "Long Bond price too low"
        if self.term < 5:
            return "Lond Bond term too short"
        if self.i_rate < 0.03:
            return"Long Bond interest rate too small"
        #Need a way to break the (whole) script if one of the above conditions are met
        else:
            return "Long term bond requirements met"






###################################################################
                        #TESTS#
###################################################################

################Testing Compound Interest Calculations################

roi_1= Short_Bonds.compound_interest(8,1001,0.04)

roi_2 = Long_Bonds.compound_interest(10, 5000,0.06)
print("Short Term ROI:",roi_1)
print("Long Term ROI:", roi_2)


#################Testing minimal values################

#Short term: Term too short
print(Short_Bonds.min_values_st(Short_Bonds(1,1000, 500,0.01)))
#Short term: Ok
print(Short_Bonds.min_values_st(Short_Bonds(10,1000, 700,0.01)))

#Long Term: Interest rate too low
print(Long_Bonds.min_values_lt(Long_Bonds(20,1005,600,0.05)))
#Long term: Ok
print(Long_Bonds.min_values_lt(Long_Bonds(20,1005,6000,0.05)))


#################Testing bond type (i.e. testing the 'get alive' function#################
type_test = Bonds(11,1001,600,0.035)
type_test.set_short_term(True)
print(type_test.get_bond_type())

###################################################################
                        #Plot Evolution#
###################################################################

#define empty list/vector for both Short & Long term bonds
roi_short = []
roi_long = []
#Create loop to simulate each year
for term in range(0,50): #check starting point is correct
    print(term)
    annual_rtn_s= Short_Bonds.compound_interest(term,500,0.01)
    annueal_rtn_l= Long_Bonds.compound_interest(term,1000, 0.03)
    roi_short.append(annual_rtn_s)
    roi_long.append(annueal_rtn_l)
print("ROI Short:", roi_short)
print("ROI Long:", roi_long)

#Create plot
plt.figure(1)
plt.title("Evolution of ROI over 50 years.") #Main title not working...

#Combine both graphs into the one plot
plt.subplot(211)
plt.title("Long Term Bonds")
plt.plot(roi_long, linewidth = 4, color = 'r')

plt.subplot((212))
plt.title("Short Term Bonds")
plt.plot(roi_short, linewidth = 2)

plt.show()
